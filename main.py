import imaplib
from config import config
from dataclasses import dataclass, field
from datetime import timedelta
from datetime import datetime as dt
from typing import Dict


@dataclass
class EmailFilter:
    filter_set: Dict = field(init=False, default_factory=lambda: {
        "FROM": None,
        "TO": None,
        "BEFORE": None,
    })
    action: str = None  # so far only delete is supported
    current_folder: str = None
    destination_folder: str = None  # not used yet
    dest_folder_create_if_not_exist: bool = None  # not used yet
    search_from_address: str = None
    search_to_address: str = None
    search_older_than_days: int = None

    def __post_init__(self):
        self.filter_set['FROM'] = self.search_from_address
        self.filter_set['TO'] = self.search_to_address
        # TODO: Is this going to work regardless of the date format on the email server?
        self.filter_set['BEFORE'] =\
            f"{dt.strftime(dt.now() - timedelta(days=self.search_older_than_days), '%d-%b-%Y')}"

    def filter_string(self):
        filter_l = [f'{a} "{v}"' for a, v in self.filter_set.items() if v is not None]
        rule_string = f"({' '.join(filter_l)})"
        return rule_string


if __name__ == '__main__':
    imap_url = config['SERVER']['imap_url']
    imap_port = config['SERVER']['imap_port']
    login = config['PERSONAL']['login']
    password = config['PERSONAL']['password']
    filters = config['FILTERS']

    box = imaplib.IMAP4_SSL(imap_url, imap_port)
    box.login(login, password)
    for counter, f in enumerate(filters.keys()):
        email_filter = EmailFilter(
            action=filters[f].get('action', None),
            current_folder=filters[f].get('current_folder', None),
            destination_folder=filters[f].get('destination_folder', None),
            dest_folder_create_if_not_exist=filters[f].get('dest_folder_create_if_not_exist', None),
            search_from_address=filters[f].get('search_from_address', None),
            search_to_address=filters[f].get('search_to_address', None),
            search_older_than_days=filters[f].get('search_older_than_days', None),
        )

        box.select(email_filter.current_folder)
        search_filter = email_filter.filter_string()
        print(search_filter)
        typ, msgnums = box.search(None, search_filter)
        for num in msgnums[0].split():
            box.store(num, '+FLAGS', '\\Deleted')
            print(f"Deleted {counter}", end='\r')

    box.expunge()
    box.close()
    box.logout()

