**This deletes email. Use with caution.**
- A fairly rudimentary implementation of https://docs.python.org/3.7/library/imaplib.html
- Removes anything from a given address within the date range
- **Some** testing has been done, but not much
- Before use `cp default_config.py config.py` and edit `config.py` as needed